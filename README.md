# Disjunctive Logic Programming

The programming API created by University of Silesia's Answer Set Programming Research Group.

The documention can be found [here](https://answer-set-programming.gitlab.io/disjunctive-logic-programming/).

You can easily add this code as a dependency in your project by using [jitpack.io](https://jitpack.io/#com.gitlab.answer-set-programming/disjunctive-logic-programming).
Protip: get sources and javadoc as well, read [this article](https://www.mkyong.com/maven/maven-get-source-code-for-jar/) or [this thread](https://stackoverflow.com/questions/5780758/maven-always-download-sources-and-javadocs) to learn how to do it. 
