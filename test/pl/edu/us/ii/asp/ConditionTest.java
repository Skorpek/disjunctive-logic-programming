package pl.edu.us.ii.asp;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.lang.reflect.Field;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class ConditionTest  {

	@ParameterizedTest
	@CsvSource({ "1, 0, true", "0, 0, false", "0, 1, false" })
	public void testConditionFunction(String args1, String args2, boolean expected) {
		ConditionFunction predicate = (String ...args) -> Integer.valueOf(args[0]) > Integer.valueOf(args[1]);
		Condition condition = new Condition("(a > b)", predicate, args1, args2);
		assertEquals(expected, condition.test());
	}
	
	@Test
	public void testAutonamedConditions() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		Field field = Condition.class.getDeclaredField("id");
		field.setAccessible(true);
		field.set(null, 1);
		ConditionFunction predicate = (String ...args) -> Integer.valueOf(args[0]) > Integer.valueOf(args[1]);
		Condition condition1 = new Condition(predicate);
		Condition condition2 = new Condition(predicate);
		assertEquals("condition#1()", condition1.toString());
		assertEquals("condition#2()", condition2.toString());
	}
	
}
