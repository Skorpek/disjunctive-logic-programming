package pl.edu.us.ii.asp.algorithms;

import pl.edu.us.ii.asp.Program;

public interface GroundingAlgorithm {

	public Program ground(Program program);
	
}
