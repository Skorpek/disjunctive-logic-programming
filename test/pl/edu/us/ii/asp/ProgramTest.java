package pl.edu.us.ii.asp;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import static pl.edu.us.ii.asp.DLP.*;

public class ProgramTest  {

	@Test
	public void testDefaultHeadPredicateWithEmptyNegativeRules() {
		Program program = new Program();
		program.addConstraint(Atoms());
		assertEquals(
			"__aux",
			program.getRulesAsArray()[0].getHead()[0].getPredicate()
		);
		assertEquals(
			"__aux",
			program.getRulesAsArray()[0].getNegativeAtoms()[0].getPredicate()
		);
	}
	
	@Test
	public void testDefaultHeadPredicateWithNegativeRules() {
		Program program = new Program();
		program.addConstraint(Atoms(), Atoms(Atom("X")));
		assertEquals(
			"__aux",
			program.getRulesAsArray()[0].getHead()[0].getPredicate()
		);
		assertEquals(
			"__aux",
			program.getRulesAsArray()[0].getNegativeAtoms()[1].getPredicate()
		);
	}
	
}
